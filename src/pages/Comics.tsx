import { useEffect } from "react";

//? COMPONENTS
import PageLayout from "../components/Layout/Page";
import BannerUI from "../components/UI/Banner";
import CharacterItem from "../components/UI/Character/CharacterItem";

//? HOOKS
import useHttp from "../hooks/use-http";

//? MODELS
import Characters from "../models/characters";

//? GLOBA vars
const { VITE_API_URL, VITE_TIMESTAMP, VITE_API_KEY, VITE_HASH } = import.meta
	.env;
const url = `${VITE_API_URL}/comics?${VITE_TIMESTAMP}&${VITE_API_KEY}&${VITE_HASH}`;

const ComicsPage = () => {
	const { data:comics, fetchingData } = useHttp({ url });

	useEffect(() => {
		fetchingData();
	}, []);
	const comicsList = comics.length > 0 && (
		<ul>
			{comics.map((comic: Characters) => (
				<CharacterItem item={comic} />
			))}
		</ul>
	);
	return (
		<PageLayout>
			<BannerUI />
      <section className="main-wrapper">
        {comicsList}
      </section>
		</PageLayout>
	);
};
export default ComicsPage;
