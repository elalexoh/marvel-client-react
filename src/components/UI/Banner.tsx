import heroDefault from "../../assets/images/hero-default.jpg";
import styles from "./Banner.module.css";

const BannerUI: React.FC = () => {
	return (
		<div
			className={styles["banner"]}
			style={{ backgroundImage: `url(${heroDefault})` }}
		></div>
	);
};
export default BannerUI;
