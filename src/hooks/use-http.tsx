import { parse } from "path/posix";
import { useContext, useEffect, useState } from "react";
import { json } from "stream/consumers";
import { LoadingContext } from "../context/loading-context";

const { VITE_API_URL, VITE_TIMESTAMP, VITE_API_KEY, VITE_HASH } = import.meta
	.env;

type HttpConfig = {
	method: string;
	headers: any;
	body?: any;
};
type UseHttp = {
	url: string;
	httpConf?: HttpConfig;
};

const useHttp = (params: UseHttp) => {
  const { httpConf, url } = params;
    
  const [data, setData] = useState([]);
  
	const { loading } = useContext(LoadingContext);

	const fetchingData = () => {
    loading(true);
		const response = fetch(url, httpConf ? httpConf : {})
			.then((response) => response.json())
			.then((res) => {
				loading(false);
				setData(res.data.results);
			})
			.catch((error) => {
				console.debug("ha ocurrido un error: ", error);
				loading(false);
			});
	};

	return { data, fetchingData };
};

export default useHttp;
