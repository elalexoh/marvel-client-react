import React, { FC } from "react";
import { Link } from "react-router-dom";
import Characters from "../../../models/characters";
import styles from "./CharacterItem.module.css";

const CharacterItem: FC<{ item: Characters }> = ({ item }) => {
	console.debug(item);
	return (
		<li key={item.id}>
			<Link to={`/characters/${item.id}`} className={styles["card"]}>
				<div className={styles["card"]}>
					<div
						className={styles["card-background"]}
						style={{
							backgroundImage: `url(${item.thumbnail.path}.${item.thumbnail.extension})`,
						}}
					></div>
					<div className={styles["card-head"]}>
						<h2 className={styles["card-title"]}>{item.name}</h2>
					</div>

					<div className={styles["card-content"]}>
						<div className={styles["card-short-description"]}>
              {item.description}
						</div>
					</div>
				</div>
			</Link>
		</li>
	);
};

export default CharacterItem;
