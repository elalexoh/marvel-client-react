import { useContext, useEffect, useState } from "react";
import useHttp from "../hooks/use-http";

// Components
import PageLayout from "../components/Layout/Page";
import BannerUI from "../components/UI/Banner";
import CharacterItem from "../components/UI/Character/CharacterItem";
import styles from "./Characters.module.css";

// Models
import Characters from "../models/characters";

// GLOBA vars
const { VITE_API_URL, VITE_TIMESTAMP, VITE_API_KEY, VITE_HASH } = import.meta
	.env;
const url = `${VITE_API_URL}/characters?${VITE_TIMESTAMP}&${VITE_API_KEY}&${VITE_HASH}`;

const CharactersPage = () => {
	const { data: characters, fetchingData } = useHttp({
		url,
	});

	useEffect(() => {
		fetchingData();
	}, []);

	const charactersList = characters.length > 0 && (
		<ul className={styles["list"]}>
			{characters.map((character: Characters) => (
				<CharacterItem item={character} />
			))}
		</ul>
	);
	return (
		<PageLayout>
			<BannerUI></BannerUI>
			<section className="main-wrapper">
				{charactersList}
			</section>
		</PageLayout>
	);
};
export default CharactersPage;
