import React, { useState, FC } from "react";

type Loading = {
  isLoading: boolean;
  loading: (payload: boolean) => void
};

const defaultState = {
  isLoading: false,
  loading: () => {}
};

export const LoadingContext = React.createContext<Loading>(defaultState);

export const LoadingProvider:FC = ({children}) => {
  const [isLoading, setIsLoading] = useState(false)

  const loading = (payload: boolean) => {setIsLoading(payload)}

  return <LoadingContext.Provider value={{isLoading: isLoading, loading: loading}}>
    {children}
  </LoadingContext.Provider>;
};
