import Footer from "../Core/Footer"
import MainNav from "../UI/MainNav"

const PageLayout: React.FC<{param?: any}> = (props) => {
  return <main >
    <MainNav />
    {props.children}
    <Footer />
  </main>
}


  export default PageLayout