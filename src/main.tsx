import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import { BrowserRouter } from "react-router-dom";
import {LoadingProvider} from './context/loading-context';

ReactDOM.render(
  <LoadingProvider >
    <React.StrictMode>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </React.StrictMode>
  </LoadingProvider>,
  document.getElementById('root')
)
