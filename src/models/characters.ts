class Thumbnail {
  path: string
  extension: string

  constructor(path: string, extension: string) {
    this.path = path
    this.extension = extension
  }
}
class Characters {
  id: number
  name: string
  thumbnail: Thumbnail
  description: string

  constructor(id: number, name: string, thumbnail: Thumbnail, description: string) {
    this.id = id
    this.name = name
    this.thumbnail = thumbnail
    this.description = description

  }
}

export default Characters