import PageLayout from "../components/Layout/Page";
import BannerUI from "../components/UI/Banner";

const SeriesPage = () => {
	return (
		<PageLayout>
			<BannerUI />
			<section className="main-wrapper">main container</section>
		</PageLayout>
	);
};
export default SeriesPage;
