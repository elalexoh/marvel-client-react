import React from "react";
import { useParams } from "react-router-dom";

const CharacterPage = () => {
  const params = useParams()
	return <div>CharacterPage {params.characterId} </div>;
};

export default CharacterPage;
