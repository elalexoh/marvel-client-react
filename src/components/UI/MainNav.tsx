import { Link } from "react-router-dom"
import styles from './MainNav.module.css'
import brandLogo from '../../assets/images/brand-logo.png'

const MainNav = () => {
  return <nav className={styles['main-nav']}>
    <Link to='/'>
      <figure className={styles['brand-wrapper']}>
        <img className={styles['main-nav__brand-logo']} src={brandLogo} alt="" />
      </figure>
    </Link>
    <ul className={styles['main-nav__items']}>
      <li className={styles['main-nav__item']}>
        <Link to='/characters'>Characters</Link>
      </li>
      <li className={styles['main-nav__item']}>
        <Link to='/comics'>Comics</Link></li>
      <li className={styles['main-nav__item']}>
        <Link to='/creators'>Creators</Link></li>
      <li className={styles['main-nav__item']}>
        <Link to='/Series'>Series</Link></li>
    </ul>

  </nav>
}
export default MainNav