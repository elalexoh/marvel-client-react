import React from 'react'
import loader from '../../assets/images/loader.svg'
import styles from './Loading.module.css'

const Loading = () => {
  return (
    <div className={styles['loading-wrapper']}>
      <img src={loader} alt="" />
    </div>
  )
}

export default Loading