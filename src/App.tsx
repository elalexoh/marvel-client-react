import { useContext, useState } from 'react'
import { Route, Routes } from 'react-router-dom'
import './App.css'
import CharactersPage from './pages/Characters'
import CharacterPage from './pages/Character'
import ComicsPage from './pages/Comics'
import CreatorsPage from './pages/Creators'
import SeriesPage from './pages/Series'
import Loading from './components/UI/Loading'
import { LoadingContext } from './context/loading-context'

function App() {
  const {isLoading} = useContext(LoadingContext)

  return (
    <div className="App">
      {isLoading && <Loading />}
      <Routes>
        <Route path="/" element={<CharactersPage />} />
        <Route path="/characters" element={<CharactersPage />} />
        <Route path="/characters/:characterId" element={<CharacterPage />} />
        <Route path="/comics" element={<ComicsPage />} />
        <Route path="/creators" element={<CreatorsPage />} />
        <Route path="/series" element={<SeriesPage />} />
      </Routes>
    </div>
  )
}

export default App
